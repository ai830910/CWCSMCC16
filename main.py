from mapreduce import MapReduce
from operator import itemgetter
import math
import datetime
import argparse


class FlightCount(MapReduce):

    def mapper(self, value):
        return [(value[2], value[1]), (value[3], value[1])]

    def reducer(self, key, values):
        return key, len(set(values))

    def write_to_file(self):
        output_file = open(self.output_dir + "/Output_FlightCount.txt", 'w')
        not_used_airports = set(self.airport_data.keys())
        output_file.write("The number of flight from each airport\n")
        output_file.write("%25s\t%10s\n"%("Airport", "#flights"))
        for output in self.final_output:
            output_file.write("%18s (%3s)\t%10s\n"%(self.airport_data[output[0]][0], output[0], output[1]))
            not_used_airports = not_used_airports - {output[0]}

        output_file.write("\nList of airport(s) not used\n")
        for air in not_used_airports:
            output_file.write("%s (%s)\n" % (self.airport_data[air][0], air))
        output_file.close()
        self.write_error()


class FlightList(MapReduce):

    def mapper(self, value):
        dep_time = datetime.datetime.utcfromtimestamp(int(value[4]))
        arr_time = dep_time + datetime.timedelta(minutes=int(value[5]))
        return [(value[1], [value[0], value[2], value[3], dep_time.strftime('%H:%M:%S'), arr_time.strftime('%H:%M:%S'), value[5]])]

    def reducer(self, key, values):
        passids = list(set([value[0] for value in values]))
        dep_air = values[0][1]
        arr_air = values[0][2]
        dep_time = values[0][3]
        arr_time = values[0][4]
        flight_times = values[0][5]
        return key, [passids, dep_air, arr_air, dep_time, arr_time, flight_times]

    def write_to_file(self):
        output_file = open(self.output_dir + "/Output_FlightList.txt", 'w')
        output_file.write("List of flights\n")

        for key, value in self.final_output:
            output_file.write("Flight id: %s\n" % key)
            output_file.write("Passenger id(s): %s\n" % (', '.join(value[0])))
            output_file.write("From airport: %s\n" % (value[1]))
            output_file.write("Destination airport: %s\n" % (value[2]))
            output_file.write("Departure time (HH:MM:SS): %s\n" % (value[3]))
            output_file.write("Arrival time (HH:MM:SS): %s\n" % (value[4]))
            output_file.write("Flight time: %d hr. %d min.\n\n" % (int(value[5])//60, int(value[5])%60))
        output_file.close()
        self.write_error()


class PassengerCount(MapReduce):

    def mapper(self, value):
        return [(value[1], value[0])]

    def reducer(self, key, values):
        return key, len(set(values))

    def write_to_file(self):
        output_file = open(self.output_dir + "/Output_PassengerCount.txt", 'w')
        output_file.write("The number of passengers on each flight\n")
        output_file.write("%s\t%s\n" % ("Flight id", "#passengers"))
        for output in self.final_output:
            output_file.write("%s\t%s\n" % (output[0], output[1]))
        output_file.close()
        self.write_error()


class PassengerHighestMiles(MapReduce):

    def dist(self, a, b):
        """Computes Nautical mile from a (lat1,lon1) to b (lat2,lon2)
        Parameters
            a: list consists of a latitude and longitude of the departure airport
            b: list consists of a latitude and longitude of the destination airport
        """
        lat1 = math.radians(float(a[1]))
        lon1 = math.radians(float(a[2]))
        lat2 = math.radians(float(b[1]))
        lon2 = math.radians(float(b[2]))
        m = 6371 * (10 ** 3)  # mean of earth radius

        A = math.sin((lat2 - lat1) / 2) ** 2
        B = math.sin((lon2 - lon1) / 2) ** 2 * math.cos(lat1) * math.cos(lat2)
        return 2 * (math.asin(math.sqrt(A + B))) * m / 1852  # Nautical miles

    def mapper(self, value):
        return [(value[0], self.dist(self.airport_data[value[2]], self.airport_data[value[3]]))]  # map PassengerID to the distance for each flight record

    def reducer(self, key, values):
        return self.reducer_sum(key, values)  # reduce by using summation of values with the same key

    def write_to_file(self):
        output_file = open(self.output_dir + "/Output_PassengerHighestMiles.txt", 'w')
        output_file.write("Passenger with the highest mile\n")
        output_file.write("%s\t%s\n" % ("Passenger id", "Total miles"))
        highest_miles = max(self.final_output, key=itemgetter(1))
        output_file.write("%s\t%s\n" % (highest_miles[0], highest_miles[1]))
        output_file.close()
        self.write_error()


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('--obj', type=str, default="1", help="objective")
    parser.add_argument('--input_filename', type=str, default="AComp_Passenger_data.csv", help="input file name")
    parser.add_argument('--input_dir', type=str, default="input_files", help="input files directory")
    parser.add_argument('--output_dir', type=str, default="output_files", help='output files directory')
    parser.add_argument('--n_mappers', type=int, default=4, help="the number of mappers")
    parser.add_argument('--n_reducers', type=int, default=4, help="the number of mappers")
    parser.add_argument('--clean', type=bool, default=True, help="clean temporal files after finish")
    parser.add_argument('--sort', type=bool, default=True, help="sort output by key")
    parser.add_argument('--descending', type=bool, default=False, help="sort by descending order")
    args = parser.parse_args()

    if args.obj == "1":
        job = FlightCount(args.input_filename, args.input_dir, args.output_dir, args.n_mappers, args.n_reducers, args.clean, args.sort, args.descending)
    elif args.obj == "2":
        job = FlightList(args.input_filename, args.input_dir, args.output_dir, args.n_mappers, args.n_reducers, args.clean, args.sort, args.descending)
    elif args.obj == "3":
        job = PassengerCount(args.input_filename, args.input_dir, args.output_dir, args.n_mappers, args.n_reducers, args.clean, args.sort, args.descending)
    elif args.obj == "4":
        job = PassengerHighestMiles(args.input_filename, args.input_dir, args.output_dir, args.n_mappers, args.n_reducers, args.clean, args.sort, args.descending)
    else:
        print("There is no objective %s" % args.obj)

    job.run()
    job.write_to_file()
    print("Completed")
