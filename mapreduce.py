import os
import json
import re
from threading import Thread

def get_input_split_file(input_dir, index):
    return input_dir + "/file_" + str(index) + ".txt"


def get_output_file(output_dir, index):
    return output_dir + "/reduce_file_" + str(index) + ".txt"


def get_output_join_file(output_dir):
    return output_dir + "/output" + ".txt"


def get_temp_map_file(output_dir, index, reducer):
    return output_dir + "/map_file_" + str(index) + "-" + str(reducer) + ".txt"


class FileHandler(object):
    """FileHandler class: Manage splitting input files and joining outputs together."""
    def __init__(self, input_filename, input_dir, output_dir):
        """
        Parameters:
            input_filename: name of an input file
            input_dir: input directory path
            output_dir: output directory path
        """
        self.input_file_path = input_dir + '/' + input_filename
        self.input_dir = input_dir
        self.output_dir = output_dir

    def initiate_file_split(self, split_index, index):
        """initialize a split file by opening and adding an index.
        Parameters:
            split_index: the split index we are currently on, to be used for naming the file.
            index: the index which is the line number in the original file.
        """
        file_split = open(get_input_split_file(self.input_dir, split_index-1), "w+")
        file_split.write(str(index) + "\n")
        return file_split

    def split_file(self, number_of_splits):
        """split a file into multiple files.
        Parameters:
            number_of_splits: the number of chunks to split the file into.
        """
        original_file = open(self.input_file_path, "r")
        file_content = original_file.readlines()
        original_size = len(file_content)
        unit_size = original_size / number_of_splits + 1
        original_file.close()
        (index, current_split_index) = (1, 1)
        current_split_unit = self.initiate_file_split(current_split_index, index)  # start with the first split file
        for line in file_content:
            current_split_unit.write(line)
            if index > unit_size*current_split_index+1:  # split a file when the number of lines exceed the capacity of a split file
                current_split_unit.close()
                current_split_index += 1
                current_split_unit = self.initiate_file_split(current_split_index, index)  # start the next split file
            index += 1
        current_split_unit.close()
        
    def join_files(self, number_of_files, clean, sort, descending):
        """join all the files in the output directory into a single output file.
        Parameters:
            number_of_files: total number of files.
            clean: if True the reduce outputs will be deleted. (default is True)
            sort: sort the outputs. (default is true)
            descending: sort by descending order. (default is True)
        Return:
            output_join_list: a list of the outputs
        """
        output_join_list = []
        for reducer_index in range(0, number_of_files):
            f = open(get_output_file(self.output_dir, reducer_index), "r")
            output_join_list += json.load(f)
            f.close()
            if clean:
                os.unlink(get_output_file(self.output_dir, reducer_index))

        if sort:
            from operator import itemgetter
            # sort using the key
            output_join_list.sort(key=itemgetter(0), reverse=descending)

        output_join_file = open(get_output_join_file(self.output_dir), "w+")
        json.dump(output_join_list, output_join_file)
        output_join_file.close()
        return output_join_list


class MapReduce(object):

    def __init__(self, input_filename, input_dir, output_dir, n_mappers, n_reducers, clean, sort, descending):
        """
        Parameters:
            input_filename: name of the input file
            input_dir: directory of the input file (default is folder "input_files" in the same directory)
            output_dir: directory of the output files (default is folder "output_files" in the same directory)
            n_mappers: number of mapper threads to use (default is 1)
            n_reducers: number of reducer threads to use (default is 1)
            clean: clean temporal files after finish (default is True)
            sort: sort output by key (default is True)
            descending: sort by descending order (default is False)
        """
        self.input_filename = input_filename
        self.input_dir = input_dir
        self.output_dir = output_dir
        self.n_mappers = n_mappers
        self.n_reducers = n_reducers
        self.clean = clean
        self.sort = sort
        self.descending = descending
        self.file_handler = FileHandler(self.input_filename, self.input_dir, self.output_dir)
        self.file_handler.split_file(self.n_mappers)
        self.error_record = {"e1": list(), "e2": list()}
        self.airport_data = self.load_airport_data()

    def mapper(self, value):
        """Mapper method needed to be overridden when implementing a particular MapReduce class"""
        pass

    def reducer(self, key, values):
        """Reducer method needed to be overridden when implementing a particular MapReduce class"""
        pass

    def reducer_sum(self, key, values):
        """Simple reducer using a summation of all values with the same key
        Parameters:
            key: key obtained from the mapper
            values: list of value with the same key obtained from mapper
        Return:
            key: key obtained from the mapper
            sum_value: summation of all values
        """
        sum_value = sum(value for value in values)
        return key, sum_value

    def check_position(self, key, position):
        """Checks the right position before writing the mapper result"""
        return position == (hash(key) % self.n_reducers)

    def run_mapper(self, index):
        """Runs the implemented mapper
        Parameters:
            index: the index of the thread to run on
        """
        input_split_file = open(get_input_split_file(self.input_dir, index), "r")
        split_file_index = input_split_file.readline()

        mapper_result = []
        for line in input_split_file:
            line = line.upper()
            if self.is_valid(line):
            # run mapper by sending key as a identifying number in a split file
                [mapper_result.append(result) for result in self.mapper(line.strip().split(','))]

        input_split_file.close()
        if self.clean:
            os.unlink(get_input_split_file(self.input_dir, index))
        for reducer_index in range(self.n_reducers):
            temp_map_file = open(get_temp_map_file(self.output_dir, index, reducer_index), "w+")
            json.dump([(key, value) for (key, value) in mapper_result if self.check_position(key, reducer_index)], temp_map_file)
            temp_map_file.close()
        
    def run_reducer(self, index):
        """Runs the implemented reducer
        Parameters:
            index: the index of the thread to run on
        """
        key_values_map = {}
        for mapper_index in range(self.n_mappers):
            temp_map_file = open(get_temp_map_file(self.output_dir, mapper_index, index), "r")
            mapper_results = json.load(temp_map_file)
            for (key, value) in mapper_results:
                if not(key in key_values_map):
                    key_values_map[key] = []
                try:
                    key_values_map[key].append(value)
                except Exception:
                    print("Exception while inserting key: ", key)
            temp_map_file.close()
            if self.clean:
                os.unlink(get_temp_map_file(self.output_dir, mapper_index, index))
        key_value_list = []
        for key in key_values_map:
            key_value_list.append(self.reducer(key, key_values_map[key]))
        output_file = open(get_output_file(self.output_dir, index), "w")
        json.dump(key_value_list, output_file)
        output_file.close()

    def run(self):
        """Executes the map and reduce operations"""
        # initialize mappers list
        map_workers = []
        # initialize reducers list
        rdc_workers = []
        # run the map step
        for thread_id in range(self.n_mappers):
            t = Thread(target=self.run_mapper, args=(thread_id,))
            t.start()
            map_workers.append(t)
        [t.join() for t in map_workers]
        # run the reduce step
        for thread_id in range(self.n_reducers):
            t = Thread(target=self.run_reducer, args=(thread_id,))
            t.start()
            rdc_workers.append(t)
        [t.join() for t in rdc_workers]

        self.final_output = self.file_handler.join_files(self.n_reducers, self.clean, self.sort, self.descending)

    def load_airport_data(self):
        """Loads airport data from Top30_airports_LatLong.csv
        Return:
            airport_data: a dictionary with an airport code as a key and other details are store in a list
        """
        airport_data = dict()
        with open("aux_files/Top30_airports_LatLong.csv", 'r') as f:
            for line in f:
                values = line.strip().split(',')
                if len(values) == 4:
                    airport_data[values[1]] = [values[0], values[2], values[3]]
        return airport_data


    def is_valid(self, line):
        """Handles input error by checking each data format
        Parameters
            line: a single line of input consists of five elements
        """
        pat = dict()
        pat[0] = "[A-Z]{3}[0-9]{4}[A-Z]{2}[0-9]"
        pat[1] = "[A-Z]{3}[0-9]{4}[A-Z]"
        pat[2] = "[A-Z]{3}"
        pat[3] = "[A-Z]{3}"
        pat[4] = "[0-9]{10}"
        pat[5] = "[0-9]{1,4}"
        data = line.strip().split(',')
        if not all([re.fullmatch(pat[i], data[i]) for i in range(len(data))]):
            self.error_record["e1"].append(line)
            return False
        if not (data[2] in self.airport_data.keys() and data[3] in self.airport_data.keys()):
            self.error_record["e2"].append(line)
            return False
        return True

    def write_error(self):
        """Writes "Error_record" file where errors are reported with messages behind input lines with an error"""
        input_file = open(self.input_dir + '/' + self.input_filename, 'r')
        error_file = open(self.output_dir + "/Error_record.csv", "w")
        for line in input_file:
            line = line.upper()
            if line in self.error_record["e1"]:
                line = line.strip('\n') + ",Error: Incorrect input format\n"
            elif line in self.error_record["e2"]:
                line = line.strip('\n') + ",Error: Airport code not found\n"
            error_file.write(line)
        input_file.close()
        error_file.close()


